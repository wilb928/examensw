/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexiones;

import Modelos.Contenido;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Wilb928
 */
@Stateless
public class ContenidoFacade extends AbstractFacade<Contenido> {
    @PersistenceContext(unitName = "ExamenSWPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContenidoFacade() {
        super(Contenido.class);
    }
    
}
