/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Wilb928
 */
@Entity
@Table(name = "contrato")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrato.findAll", query = "SELECT c FROM Contrato c"),
    @NamedQuery(name = "Contrato.findById", query = "SELECT c FROM Contrato c WHERE c.id = :id"),
    @NamedQuery(name = "Contrato.findByCostoTotal", query = "SELECT c FROM Contrato c WHERE c.costoTotal = :costoTotal"),
    @NamedQuery(name = "Contrato.findByACuenta", query = "SELECT c FROM Contrato c WHERE c.aCuenta = :aCuenta"),
    @NamedQuery(name = "Contrato.findBySaldo", query = "SELECT c FROM Contrato c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "Contrato.findByFechaCont", query = "SELECT c FROM Contrato c WHERE c.fechaCont = :fechaCont"),
    @NamedQuery(name = "Contrato.findByObs", query = "SELECT c FROM Contrato c WHERE c.obs = :obs"),
    @NamedQuery(name = "Contrato.findByFechaEve", query = "SELECT c FROM Contrato c WHERE c.fechaEve = :fechaEve"),
    @NamedQuery(name = "Contrato.findByHoraE", query = "SELECT c FROM Contrato c WHERE c.horaE = :horaE"),
    @NamedQuery(name = "Contrato.findByAnfitrion", query = "SELECT c FROM Contrato c WHERE c.anfitrion = :anfitrion"),
    @NamedQuery(name = "Contrato.findByDetalle", query = "SELECT c FROM Contrato c WHERE c.detalle = :detalle")})
public class Contrato implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CostoTotal")
    private double costoTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACuenta")
    private double aCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Saldo")
    private double saldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaCont")
    @Temporal(TemporalType.DATE)
    private Date fechaCont;
    @Size(max = 100)
    @Column(name = "Obs")
    private String obs;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaEve")
    @Temporal(TemporalType.DATE)
    private Date fechaEve;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HoraE")
    @Temporal(TemporalType.TIME)
    private Date horaE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ANFITRION")
    private String anfitrion;
    @Size(max = 255)
    @Column(name = "DETALLE")
    private String detalle;
    @JoinColumn(name = "IdPaq", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Paquete idPaq;
    @JoinColumn(name = "CIE", referencedColumnName = "CI")
    @ManyToOne(optional = false)
    private Empleado cie;
    @JoinColumn(name = "IdRes", referencedColumnName = "Id")
    @ManyToOne
    private Reserva idRes;
    @JoinColumn(name = "IdTE", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private TipoEvento idTE;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCont")
    private List<Cobros> cobrosList;

    public Contrato() {
    }

    public Contrato(Integer id) {
        this.id = id;
    }

    public Contrato(Integer id, double costoTotal, double aCuenta, double saldo, Date fechaCont, Date fechaEve, Date horaE, String anfitrion) {
        this.id = id;
        this.costoTotal = costoTotal;
        this.aCuenta = aCuenta;
        this.saldo = saldo;
        this.fechaCont = fechaCont;
        this.fechaEve = fechaEve;
        this.horaE = horaE;
        this.anfitrion = anfitrion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(double costoTotal) {
        this.costoTotal = costoTotal;
    }

    public double getACuenta() {
        return aCuenta;
    }

    public void setACuenta(double aCuenta) {
        this.aCuenta = aCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Date getFechaCont() {
        return fechaCont;
    }

    public void setFechaCont(Date fechaCont) {
        this.fechaCont = fechaCont;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Date getFechaEve() {
        return fechaEve;
    }

    public void setFechaEve(Date fechaEve) {
        this.fechaEve = fechaEve;
    }

    public Date getHoraE() {
        return horaE;
    }

    public void setHoraE(Date horaE) {
        this.horaE = horaE;
    }

    public String getAnfitrion() {
        return anfitrion;
    }

    public void setAnfitrion(String anfitrion) {
        this.anfitrion = anfitrion;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Paquete getIdPaq() {
        return idPaq;
    }

    public void setIdPaq(Paquete idPaq) {
        this.idPaq = idPaq;
    }

    public Empleado getCie() {
        return cie;
    }

    public void setCie(Empleado cie) {
        this.cie = cie;
    }

    public Reserva getIdRes() {
        return idRes;
    }

    public void setIdRes(Reserva idRes) {
        this.idRes = idRes;
    }

    public TipoEvento getIdTE() {
        return idTE;
    }

    public void setIdTE(TipoEvento idTE) {
        this.idTE = idTE;
    }

    @XmlTransient
    public List<Cobros> getCobrosList() {
        return cobrosList;
    }

    public void setCobrosList(List<Cobros> cobrosList) {
        this.cobrosList = cobrosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id+"-"+anfitrion;
    }
    
}
